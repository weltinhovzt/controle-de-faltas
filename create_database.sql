CREATE TABLE `usuarios` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nome` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `usuario` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `perfil` int COLLATE utf8mb4_unicode_ci NOT NULL,
  `senha` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `usuarios_usuario_unique` (`usuario`);

ALTER TABLE `usuarios`
MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;


CREATE TABLE `turmas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `professor_id` bigint(20) UNSIGNED NOT NULL,
  `nome` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `numero_sala` int COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

ALTER TABLE `turmas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `turmas_professor_id_foreign` (`professor_id`);

ALTER TABLE `turmas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `turmas`
  ADD CONSTRAINT `turmas_professor_id_foreign` FOREIGN KEY (`professor_id`) REFERENCES `usuarios` (`id`);



CREATE TABLE `turma_alunos` (
  `turma_id` bigint(20) UNSIGNED NOT NULL,
  `aluno_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

ALTER TABLE `turma_alunos`
  ADD KEY `turma_alunos_turma_id_foreign` (`turma_id`),
  ADD KEY `turma_alunos_aluno_id_foreign` (`aluno_id`);

ALTER TABLE `turma_alunos`
  ADD CONSTRAINT `turma_alunos_turma_id_foreign` FOREIGN KEY (`turma_id`) REFERENCES `turmas` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `turma_alunos_aluno_id_foreign` FOREIGN KEY (`aluno_id`) REFERENCES `usuarios` (`id`) ON DELETE CASCADE;



CREATE TABLE `chamadas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `turma_id` bigint(20) UNSIGNED NOT NULL,
  `data` date COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

ALTER TABLE `chamadas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `chamadas_turma_id_foreign` (`turma_id`);

ALTER TABLE `chamadas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `chamadas`
  ADD CONSTRAINT `chamadas_turma_id_foreign` FOREIGN KEY (`turma_id`) REFERENCES `turmas` (`id`);


CREATE TABLE `chamada_alunos` (
  `chamada_id` bigint(20) UNSIGNED NOT NULL,
  `aluno_id` bigint(20) UNSIGNED NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

ALTER TABLE `chamada_alunos`
  ADD KEY `chamada_alunos_chamada_id_foreign` (`chamada_id`),
  ADD KEY `chamada_alunos_aluno_id_foreign` (`aluno_id`);

ALTER TABLE `chamada_alunos`
  ADD CONSTRAINT `chamada_alunos_chamada_id_foreign` FOREIGN KEY (`chamada_id`) REFERENCES `chamadas` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `chamada_alunos_aluno_id_foreign` FOREIGN KEY (`aluno_id`) REFERENCES `usuarios` (`id`) ON DELETE CASCADE;