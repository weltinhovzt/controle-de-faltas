<?php

//Login
$router->get('/login', 'App\Controllers\AuthController::index');
$router->post('/login', 'App\Controllers\AuthController::login');
$router->get('/logout', 'App\Controllers\AuthController::logout');

//Home
$router->get('/', 'App\Controllers\AuthController::home');


//Usuarios
$router->get('/usuarios', 'App\Controllers\UsuarioController::index');
$router->post('/usuarios', 'App\Controllers\UsuarioController::store');
$router->get('/usuarios/create', 'App\Controllers\UsuarioController::create');
$router->get('/usuarios/{id}/edit', 'App\Controllers\UsuarioController::edit');
$router->post('/usuarios/{id}', 'App\Controllers\UsuarioController::update');
$router->post('/usuarios/destroy/{id}', 'App\Controllers\UsuarioController::destroy');

//Turmas
$router->get('/turmas', 'App\Controllers\TurmaController::index');
$router->post('/turmas', 'App\Controllers\TurmaController::store');
$router->get('/turmas/create', 'App\Controllers\TurmaController::create');
$router->get('/turmas/{id}/edit', 'App\Controllers\TurmaController::edit');
$router->post('/turmas/{id}', 'App\Controllers\TurmaController::update');
$router->get('/turmas/{id}', 'App\Controllers\TurmaController::show');
$router->post('/turmas/destroy/{id}', 'App\Controllers\TurmaController::destroy');

//Alunos
$router->get('/alunos', 'App\Controllers\AlunoController::index');
$router->post('/alunos', 'App\Controllers\AlunoController::store');
$router->get('/alunos/create', 'App\Controllers\AlunoController::create');
$router->get('/alunos/{id}/edit', 'App\Controllers\AlunoController::edit');
$router->patch('/alunos/{id}', 'App\Controllers\AlunoController::update');
$router->post('/alunos/destroy', 'App\Controllers\AlunoController::destroy');

//Chamadas
$router->get('/chamadas', 'App\Controllers\ChamadaController::index');
$router->post('/chamadas/{id}', 'App\Controllers\ChamadaController::store');
$router->get('/chamadas/{id}/create', 'App\Controllers\ChamadaController::create');
$router->get('/chamadas/{id}/edit', 'App\Controllers\ChamadaController::edit');
$router->get('/chamadas/{id}', 'App\Controllers\ChamadaController::show');
$router->patch('/chamadas/{id}', 'App\Controllers\ChamadaController::update');
$router->post('/chamadas/destroy', 'App\Controllers\ChamadaController::destroy');
$router->get('/chamadas/registradas', 'App\Controllers\ChamadaController::registradas');


//Historico
$router->get('/historico', 'App\Controllers\HistoricoController::index');
