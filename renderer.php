<?php

use App\Middleware\Auth;

$loader = new \Twig\Loader\FilesystemLoader(__DIR__ . '/resources/view');
$twig = new \Twig\Environment($loader, [
    //'cache' => __DIR__ . '/cache',
]);

$twig->addGlobal('BASE_HREF', 'http://localhost:8000');
$twig->addGlobal('user', Auth::user());


return $twig;
