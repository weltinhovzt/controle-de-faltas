const mix = require('laravel-mix');

mix.combine([
  'node_modules/bootstrap/dist/css/bootstrap.min.css',
  'resources/assets/style.css',
], 'public/css/admin.css');


mix.combine([
  'node_modules/jquery/dist/jquery.min.js',
  'node_modules/bootstrap/dist/js/bootstrap.min.js',
  ], 'public/js/admin.js');

  mix.combine([
    'node_modules/bootstrap/dist/css/bootstrap.min.css',
    'resources/assets/dashboard.css',
  ], 'public/css/app.css');
  
  
  mix.combine([
    'node_modules/jquery/dist/jquery.min.js',
    'node_modules/bootstrap/dist/js/bootstrap.min.js',
    'node_modules/feather-icons/dist/feather.min.js',
    ], 'public/js/app.js');
  
