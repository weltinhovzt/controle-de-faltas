<?php

namespace App\Middleware;

use App\Models\Usuario;

class Auth
{
    public function __construct()
    {

    }

    public function check()
    {
        if(isset($_SESSION['id'])){
            return true;
        }else {
            return header("Location: http://localhost:8000/login");
        }
    }

    public function user()
    {   
        if(isset($_SESSION['id'])){
            return Usuario::where('id', $_SESSION['id'])->with('turmas')->first();
        }
    }

}
