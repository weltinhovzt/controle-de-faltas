<?php

namespace App\Controllers;

use App\Middleware\Auth;
use App\Models\Turma;
use App\Models\Usuario;
use Twig\Environment;

class TurmaController
{
    private $twig;

    public function __construct(Environment $twig)
    {
        $this->twig = $twig;

        Auth::check();

        if(Auth::user()->perfil != "Administrador"){
            return header("Location: http://localhost:8000/login");
        }
    }

    public function index()
    {   
        $title = "Consultar Turmas";
        $turmas = Turma::with('professor')->get();

        return $this->twig->render('turmas/index.html', ['title' => $title, 'turmas' => $turmas]);
    }

    public function create()
    {
        $title = "Cadastrar Turma";
        $professores = Usuario::where('perfil', 2)->with('turma')->get();

        return $this->twig->render('turmas/create.html', ['title' => $title, 'professores' => $professores]);
    }

    public function edit($id)
    {
        $title = "Editar Turma";
        $professores = Usuario::where('perfil', 2)->with('turma')->get();
        $turma = Turma::with('professor', 'alunos')->find($id)->first();

        return $this->twig->render('turmas/edit.html', ['title' => $title, 'professores' => $professores, 'turma' => $turma]);
    }

    public function store()
    {   
        try {
            Turma::create([
                'professor_id'  => $_REQUEST['professor_id'],
                'nome'          => $_REQUEST['nome'],
                'numero_sala'   => $_REQUEST['numero_sala']
            ]);

            $result = array(
                'titulo'     => 'Sucesso!',
                'mensagem'   => 'Turma cadastrada com sucesso.'
            );
    
            return json_encode($result);
        }
        catch (\Exception $e){

            $result = array(
                'titulo'     => 'Error!',
                'mensagem'   => 'Verifique os dados digitados e tente novamente.'. $e
            );

            http_response_code(401);

            return json_encode($result);
        }

    }

    public function update($id)
    {
        try {

            $turma = Turma::find($id)->first();

            $turma->update([
                'professor_id'  => $_REQUEST['professor_id'],
                'nome'          => $_REQUEST['nome'],
                'numero_sala'   => $_REQUEST['numero_sala']
            ]);

            $result = array(
                'titulo'     => 'Sucesso!',
                'mensagem'   => 'Turma atualizada com sucesso.'
            );
    
            return json_encode($result);
        }
        catch (\Exception $e){

            $result = array(
                'titulo'     => 'Error!',
                'mensagem'   => 'Verifique os dados digitados e tente novamente.'. $e
            );

            http_response_code(401);

            return json_encode($result);
        }
    }

    public function show($id)
    {
        $title = "Visualizar Turma";
        $turma = Turma::with('professor', 'alunos')->find($id)->first();

        return $this->twig->render('turmas/show.html', ['title' => $title, 'turma' => $turma]);
    }

    public function destroy($id)
    {
        
        try {

            Turma::destroy($id);

            $result = array(
                'titulo'     => 'Sucesso!',
                'mensagem'   => 'Turma atualizada com sucesso.'
            );
    
            return json_encode($result);
        }
        catch (\Exception $e){

            $result = array(
                'titulo'     => 'Error!',
                'mensagem'   => 'Não é possivel remover essa turma pois existe alunos vinculados com essa turma.'
            );

            http_response_code(401);

            return json_encode($result);
        }
    }
}
