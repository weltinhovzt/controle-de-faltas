<?php

namespace App\Controllers;

use App\Middleware\Auth;
use App\Models\Usuario;
use Twig\Environment;

class UsuarioController
{
    private $twig;

    public function __construct(Environment $twig)
    {
        $this->twig = $twig;
        Auth::check();

        if(Auth::user()->perfil != "Administrador"){
            return header("Location: http://localhost:8000/login");
        }
    }

    public function index()
    {   
        $title = "Consultar Usuários";
        $usuarios = Usuario::all();

        return $this->twig->render('usuarios/index.html', ['title' => $title, 'usuarios' => $usuarios]);
    }

    public function create()
    {
        $title = "Cadastrar Usuário";

        return $this->twig->render('usuarios/create.html', ['title' => $title]);
    }

    public function edit($id)
    {
        $title = "Editar Usuário";
        $usuario = Usuario::find($id)->first();

        return $this->twig->render('usuarios/edit.html', ['title' => $title, 'usuario' => $usuario]);
    }

    public function store()
    {   
        try {
            Usuario::create([
                'nome'      => $_REQUEST['nome'],
                'usuario'      => $_REQUEST['usuario'],
                'perfil'      => $_REQUEST['perfil'],
                'senha'  => password_hash($_REQUEST['senha'], PASSWORD_BCRYPT),
            ]);

            $result = array(
                'titulo'     => 'Sucesso!',
                'mensagem'   => 'Usuario cadastrado com sucesso.'
            );
    
            return json_encode($result);
        }
        catch (\Exception $e){

            $result = array(
                'titulo'     => 'Error!',
                'mensagem'   => 'Verifique os dados digitados e tente novamente.'
            );

            http_response_code(401);

            return json_encode($result);
        }

    }

    public function update($id)
    {
        try {
            $usuario = Usuario::find($id)->first();

            $usuario->update([
                'nome'      => $_REQUEST['nome'],
                'usuario'   => $_REQUEST['usuario'],
                'perfil'    => $_REQUEST['perfil'],
                'senha'     => password_hash($_REQUEST['senha'], PASSWORD_BCRYPT),
            ]);

            $result = array(
                'titulo'     => 'Sucesso!',
                'mensagem'   => 'Usuario atualizado com sucesso.'
            );
    
            return json_encode($result);
        }
        catch (\Exception $e){

            $result = array(
                'titulo'     => 'Error!',
                'mensagem'   => 'Verifique os dados digitados e tente novamente.'.$e
            );

            http_response_code(401);

            return json_encode($result);
        }
    }

    public function destroy($id)
    {
        
        try {

            Usuario::destroy($id);

            $result = array(
                'titulo'     => 'Sucesso!',
                'mensagem'   => 'Turma atualizada com sucesso.'
            );
    
            return json_encode($result);
        }
        catch (\Exception $e){

            $result = array(
                'titulo'     => 'Error!',
                'mensagem'   => 'Não é possivel remover esse usuario pois ele esta vinculado.'
            );

            http_response_code(401);

            return json_encode($result);
        }
    }
}
