<?php

namespace App\Controllers;

use App\Middleware\Auth;
use App\Models\Usuario;
use Twig\Environment;

class AuthController
{
    private $twig;

    public function __construct(Environment $twig)
    {
        $this->twig = $twig;
    }

    public function index()
    {
        return $this->twig->render('auth/index.html');
    }

    public function login()
    {   

        $usuario = Usuario::where('usuario', $_REQUEST['usuario'])->first();

        if($usuario && $usuario->validar($_REQUEST['senha'])){
            
            $_SESSION['id'] = $usuario->id;

            $result = array(
                'titulo'     => 'Sucesso!',
                'mensagem'   => 'Usuario autenticado.'
            );
    
            return json_encode($result);

        }

        $result = array(
            'titulo'     => 'Error!',
            'mensagem'   => 'Verifique os dados digitados e tente novamente.'
        );

        http_response_code(403);

        return json_encode($result);
    }

    public function logout()
    {
        session_destroy();

        return header("Location: http://localhost:8000/login");
    }

    public function home()
    {
        if (Auth::user()->perfil == "Aluno") {
            return header("Location: http://localhost:8000/historico");
        }

        if (Auth::user()->perfil == "Professor") {
            return header("Location: http://localhost:8000/chamadas");
        }

        if (Auth::user()->perfil == "Administrador") {
            return header("Location: http://localhost:8000/usuarios");
        }
    }
}
