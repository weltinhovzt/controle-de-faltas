<?php

namespace App\Controllers;

use App\Middleware\Auth;
use App\Models\Turma;
use App\Models\Chamada;
use App\Models\Usuario;
use Twig\Environment;

class ChamadaController
{
    private $twig;

    public function __construct(Environment $twig)
    {
        $this->twig = $twig;
        Auth::check();

        if (Auth::user()->perfil == "Aluno") {
            return header("Location: http://localhost:8000/login");
        }
    }

    public function index()
    {
        $title = "Realizar Chamada";

        if (Auth::user()->perfil == "Administrador") {
            $turmas = Turma::with('professor')->get();
        }

        if (Auth::user()->perfil == "Professor") {
            $turmas = Turma::where('professor_id', Auth::user()->id)->with('professor')->get();
        }


        return $this->twig->render('chamadas/index.html', ['title' => $title, 'turmas' => $turmas]);
    }

    public function create($id)
    {
        $title = "Registrar Chamada";


        if (Auth::user()->perfil == "Administrador") {
            $turma = Turma::with('professor', 'alunos')->find($id)->first();
        }

        if (Auth::user()->perfil == "Professor") {
            $turma = Turma::where('professor_id', Auth::user()->id)->with('professor', 'alunos')->find($id)->first();
        }

        if (!$turma) {
            return "Voce não tem permissão para acessar.";
        }

        return $this->twig->render('chamadas/create.html', ['title' => $title, 'turma' => $turma]);
    }

    public function store($id)
    {
        try {

            $chamada = Chamada::create([
                'turma_id'  => $id[1],
                'data'      => $_REQUEST['data'],
            ]);

            foreach ($_REQUEST['status'] as $key => $status) {
                $chamada->alunos()->attach($key, ['status' => $status]);
            }

            $result = array(
                'titulo'     => 'Sucesso!',
                'mensagem'   => 'Chamada registrada com sucesso.'
            );

            return json_encode($result);
        } catch (\Exception $e) {

            $result = array(
                'titulo'     => 'Error!',
                'mensagem'   => 'Verifique os dados digitados e tente novamente.' . $e
            );

            http_response_code(401);

            return json_encode($result);
        }
    }

    public function show($id)
    {
        $title = "Chamada Realizada em ";
        $chamada = Chamada::with('turma', 'alunos')->find($id)->first();

        return $this->twig->render('chamadas/show.html', ['title' => $title, 'chamada' => $chamada]);
    }

    public function registradas()
    {
        $title = "Chamadas Realizadas";

        if (Auth::user()->perfil == "Administrador") {
            $chamadas = Chamada::with('turma')->get();
        }

        if(Auth::user()->perfil == "Professor") {
            $chamadas = Chamada::with(["turma" => function($q){
                $q->where('professor_id', '=', Auth::user()->id);
            }])->get();
        }
        

        

        return $this->twig->render('chamadas/registradas.html', ['title' => $title, 'chamadas' => $chamadas]);
    }

    public function historico()
    {
        $title = "Historico de Presença";
        $chamadas = Chamada::with('alunos')->get();

        $soma = 0;

        foreach ($chamadas as $chamada) {
            foreach ($chamada['alunos'] as $aluno){
                if($aluno['pivot']['aluno_id'] == 9){
                    $soma =+ $soma + $aluno['pivot']['status'];
                }
            }
        }

        return $this->twig->render('chamadas/historico.html', ['title' => $title, 'chamadas' => $chamadas, 'soma' => $soma]);
    }
}
