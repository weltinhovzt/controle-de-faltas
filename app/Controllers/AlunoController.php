<?php

namespace App\Controllers;

use App\Models\Turma;
use App\Models\Usuario;
use Twig\Environment;
use App\Middleware\Auth;

class AlunoController
{
    private $twig;

    public function __construct(Environment $twig)
    {
        $this->twig = $twig;
        Auth::check();
    }

    public function create()
    {
        $title = "Cadastrar Aluno na Turma";
        $turmas = Turma::all();
        $alunos = Usuario::where('perfil', 3)->with('turmas')->get();

        return $this->twig->render('alunos/create.html', ['title' => $title, 'turmas' => $turmas,'alunos' => $alunos]);
    }

    public function store()
    {   
        try {

            Turma::find($_REQUEST['turma_id'])->alunos()->attach($_REQUEST['aluno_id']);

            $result = array(
                'titulo'     => 'Sucesso!',
                'mensagem'   => 'Aluno vinculado com sucesso.'
            );
    
            return json_encode($result);
        }
        catch (\Exception $e){

            $result = array(
                'titulo'     => 'Error!',
                'mensagem'   => 'Verifique os dados digitados e tente novamente.'. $e
            );

            http_response_code(401);

            return json_encode($result);
        }

    }

    public function destroy()
    {
        try {

            Turma::find($_REQUEST['turma_id'])->alunos()->detach($_REQUEST['aluno_id']);

            $result = array(
                'titulo'     => 'Sucesso!',
                'mensagem'   => 'Aluno desvinculado com sucesso.'
            );
    
            return json_encode($result);
        }
        catch (\Exception $e){

            $result = array(
                'titulo'     => 'Error!',
                'mensagem'   => 'Verifique os dados digitados e tente novamente.'. $e
            );

            http_response_code(401);

            return json_encode($result);
        }
    }
}
