<?php

namespace App\Controllers;

use App\Middleware\Auth;
use App\Models\Turma;
use App\Models\Chamada;
use App\Models\Usuario;
use Twig\Environment;

class HistoricoController
{
    private $twig;

    public function __construct(Environment $twig)
    {
        $this->twig = $twig;
        Auth::check();

        if (Auth::user()->perfil != "Aluno") {
            return header("Location: http://localhost:8000/login");
        }
    }

    public function index()
    {
        $title = "Historico de Presença";
        
        $chamadas = Chamada::where('turma_id', Auth::user()->turmas[0]->id)->with('alunos')->get();

        $soma = 0;

        foreach ($chamadas as $chamada) {
            foreach ($chamada['alunos'] as $aluno){
                if($aluno['pivot']['aluno_id'] == Auth::user()->id){
                    $soma =+ $soma + $aluno['pivot']['status'];
                }
            }
        }

        return $this->twig->render('historico/index.html', ['title' => $title, 'chamadas' => $chamadas, 'soma' => $soma]);
    }
}
