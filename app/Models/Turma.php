<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Turma extends Model
{
    protected $fillable = [
        'professor_id',
        'nome',
        'numero_sala',
    ];

    public function professor()
    {
        return $this->hasOne(Usuario::class, 'id', 'professor_id');
    }

    public function alunos()
    {
        return $this->belongsToMany(Usuario::class, 'turma_alunos', 'turma_id', 'aluno_id');
    }

    public function chamadas()
    {
        return $this->belongsToMany(Usuario::class, 'chamada_alunos', 'chamada_id', 'aluno_id');
    }

}
