<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    protected $fillable = [
        'nome',
        'usuario',
        'perfil',
        'senha',
    ];

    protected $hidden = [
        'senha',
    ];

    public function getPerfilAttribute($value)
    {   
        switch ($this->attributes['perfil']) {
            case 1:
                return "Administrador";
                break;
            case 2:
                return "Professor";
                break;
            case 3:
                return "Aluno";
                break;
        }  
    }

    public function turma()
    {
        return $this->hasOne(Turma::class, 'professor_id', 'id');
    }

    public function turmas()
    {
        return $this->belongsToMany(Turma::class, 'turma_alunos', 'aluno_id', 'turma_id')->with('professor');
    }

    public function validar($senha)
    {   
        if (password_verify($senha, $this->senha)) {
            return true;
        }else {
            return false;
        }
            
    }

    
}
