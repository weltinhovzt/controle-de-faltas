<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Chamada extends Model
{
    protected $fillable = [
        'turma_id',
        'data',
    ];
    
    public function turma()
    {
        return $this->hasOne(Turma::class, 'id', 'turma_id')->with('professor');
    }

    public function alunos()
    {
        return $this->belongsToMany(Usuario::class, 'chamada_alunos', 'chamada_id', 'aluno_id')->withPivot('status');
    }
}
